package com.practice;

import javafx.application.*;
import javafx.event.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.text.Text;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.fxml.FXML;

public class RpsApp extends Application {
    private RpsGame backEnd;
    
    public RpsApp() {
        this.backEnd = new RpsGame();
    }

    public void start(Stage stage) {
        Group root = new Group();

        Scene exteriorScene = new Scene(root, 650, 300);
        VBox vbox = new VBox();
        HBox buttons = new HBox();
        HBox textFields = new HBox();

        Button b1 = new Button("Rock 💎");
        Button b2 = new Button("Paper 📃");
        Button b3 = new Button("scissors ✂ ");

        TextField welcome = new TextField("Welcome!!!");
        TextField wins = new TextField("wins: ");
        TextField losses = new TextField("losses: ");
        TextField ties = new TextField("ties: ");

        

        exteriorScene.setFill(Color.BLACK);

        stage.setTitle("Rock Paper Scissors");
        stage.setScene(exteriorScene);
        //TODO: maybe this will create logical error where we cant modify the value of wins/lose/tie
        welcome.setEditable(false);
        wins.setEditable(false);
        losses.setEditable(false);
        ties.setEditable(false);
        
        welcome.setPrefWidth(200);
        wins.setPrefWidth(200);
        losses.setPrefWidth(200);
        ties.setPrefWidth(200);

        buttons.getChildren().addAll(b1,b2,b3);
        textFields.getChildren().addAll(welcome, wins, losses, ties);
        vbox.getChildren().addAll(buttons,textFields);
        root.getChildren().add(vbox);


        b1.setOnAction(new RpsChoice(welcome, wins, losses, ties, "rock", backEnd));
        b2.setOnAction(new RpsChoice(welcome, wins, losses, ties, "paper", backEnd));
        b3.setOnAction(new RpsChoice(welcome, wins, losses, ties, "scissors", backEnd));

        stage.show();
    }

    public static void main(String[] args) {
        Application.launch(args);
    }

}
