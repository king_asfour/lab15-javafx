package com.practice;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

public class RpsChoice implements EventHandler<ActionEvent> {
    private TextField message;
    private TextField win;
    private TextField losses;
    private TextField ties;
    private String playerChoice;
    private RpsGame backEnd;

    public RpsChoice(TextField message, TextField win, TextField losses, TextField ties, String playerChoice,
            RpsGame backEnd) {
        this.message = message;
        this.win = win;
        this.losses = losses;
        this.ties = ties;
        this.playerChoice = playerChoice;
        this.backEnd = backEnd;
    }

    @Override
    public void handle(ActionEvent e){
        String whoWon = this.backEnd.playARound(this.playerChoice);
        this.message.setText(whoWon);
        this.win.setText("Wins: " + Integer.toString(this.backEnd.getWins()));
        this.losses.setText("Losses: " + Integer.toString(this.backEnd.getLosses()));
        this.ties.setText("Ties: " + Integer.toString(this.backEnd.getTie()));
    }

}
