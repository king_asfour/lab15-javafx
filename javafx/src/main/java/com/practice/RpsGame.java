package com.practice;

import java.util.Random;

public class RpsGame {
    private int wins;
    private int tie;
    private int losses;

    public RpsGame() {
        this.wins = 0;
        this.tie = 0;
        this.losses = 0;
    }

    public int getWins() {
        return wins;
    }
    public int getTie() {
        return tie;
    }
    public int getLosses() {
        return losses;
    }

    public String playARound(String choice) {
        Random r = new Random();
        int randomNum = r.nextInt(3);
        String randomStringValue = "";
        if(randomNum == 0) {
            randomStringValue = "rock";
        } else if(randomNum == 1) {
            randomStringValue = "paper";
        } else if (randomNum == 2) {
            randomStringValue = "scissors";
        }

        if(randomStringValue.equals("rock") && choice.equals("scissors")) {
            this.losses++;
            return "Computer played rock. Computer Wins";
        } else if (randomStringValue.equals("paper") && choice.equals("rock")) {
            this.losses++;
            return "Computer played paper. Computer Wins";
        } else if (randomStringValue.equals("scissors") && choice.equals("paper")) {
            this.losses++;
            return "Computer played scissors. Computer wins";
        } else if (randomStringValue.equals(choice)) {
            this.tie++;
            return "Its a tie";   
        } else {
            this.wins++;
            return "YOU WIN!";
        }
    }
}
